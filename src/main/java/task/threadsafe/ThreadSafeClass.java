package task.threadsafe;

import lombok.Getter;
import lombok.NonNull;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;

public class ThreadSafeClass<K,V> {

    private ExecutorService executor = Executors.newCachedThreadPool();

    @Getter
    private Map<K, V> cache = new ConcurrentHashMap<>();

    public Future<V> compute(@NonNull K k, @NonNull Function<K, V> f) {
        return executor.submit(() -> cache.computeIfAbsent(k, f));
    }

}
