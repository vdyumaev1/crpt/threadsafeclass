package task.threadsafe;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class ThreadSafeClassTest {

    private ThreadSafeClass threadSafeClass;

    @Test
    void testCompute() throws ExecutionException, InterruptedException {
        threadSafeClass = new ThreadSafeClass<Long, Long>();

        Future<Long> result1 = threadSafeClass.compute(123L, (k) -> 555L);
        Future<Long> result2 = threadSafeClass.compute(124L, (k) -> 666L);
        Future<Long> result3 = threadSafeClass.compute(123L, (k) -> 777L);

        assertThat(threadSafeClass.getCache().size()).isEqualTo(2);
        assertThat(result1.get()).isSameAs(result3.get());
    }

}